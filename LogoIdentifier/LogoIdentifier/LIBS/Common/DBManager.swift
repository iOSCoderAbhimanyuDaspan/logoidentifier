//
//  DBManage.swift
//  LogoIdentifier
//
//  Created by Abhimanyu Rathore on 10/04/21.
//

import Foundation
enum UserStoreKeys:String{
    case game = "game"
}

public class DBManager {
    
  
    //MARK:- 1. Fetch Saved Game from DB if Exist
    public  class func getSavedGame()-> LogoGameConfiguration? {
        var savedGame:LogoGameConfiguration?
        let userDefaults = UserDefaults.standard
        do {
            savedGame = try userDefaults.getObject(forKey: UserStoreKeys.game.rawValue, castTo: LogoGameConfiguration.self)
        } catch {
            print(error.localizedDescription)
        }
        if let _savedGame  = savedGame {
            return _savedGame
        }else{
            return nil
        }
    }
    

    //MARK:- 2.Save/Update Game
    public class func saveGame(game:LogoGameConfiguration){
        let userDefaults = UserDefaults.standard
        do {
            try userDefaults.setObject(game, forKey:  UserStoreKeys.game.rawValue)
        } catch {
            print(error.localizedDescription)
        }
    }
    
    
}


