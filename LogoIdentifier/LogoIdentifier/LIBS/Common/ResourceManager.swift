//
//  Utility.swift
//  LogoIdentifier
//
//  Created by Abhimanyu Rathore on 10/04/21.
//

/*
 [
     {
     "imgUrl": "http:www.dsource.in/sites/default/files/resource/logo-design/logos/logos-representing-india/images/01.jpeg",
     "name": "AADHAAR"
     }
 ]
 */

import Foundation

class ResourceManager {
    static let shared:ResourceManager = ResourceManager()
    
    private init(){}

    //MARK:- Read File Data
    func getListOfLogos() -> [Logo]? {
       return Bundle.main.decode([Logo].self, from: "logo.txt")
    }
}


