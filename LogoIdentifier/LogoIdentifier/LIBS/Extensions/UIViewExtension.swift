//
//  UIViewExtension.swift
//  ExOfLifeCycles
//
//  Created by Abhimanyu Rathore on 30/01/21.
//

import Foundation
import UIKit

extension UIView {

    //MARK:- Constraints Helper function 
    func setConstraints(item: Any,
                        attribute: NSLayoutConstraint.Attribute,
                        relatedBy: NSLayoutConstraint.Relation,
                        toItem: Any? = nil,
                        attributeSec: NSLayoutConstraint.Attribute,
                        multiplier: CGFloat = 1.0,
                        constant: CGFloat = 0.0,
                        paretnView:UIView){
        
        let layOut = NSLayoutConstraint.init(item: item, attribute: attribute, relatedBy: relatedBy, toItem: toItem, attribute: attributeSec, multiplier: multiplier, constant: constant)
        
        paretnView.addConstraint(layOut)
    }
}


