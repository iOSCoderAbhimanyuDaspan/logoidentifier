//
//  GameLevel.swift
//  LogoIdentifier
//
//  Created by Abhimanyu Rathore on 10/04/21.
//

//eack pack have 5 levels
import Foundation
class GameLevel:Codable {
    var name: String
    var isLocked:Bool
    init(name: String = "Level 1",isLocked:Bool = true ){
        self.name = name
        self.isLocked = isLocked
    }
    
    private enum CodingKeys: String, CodingKey{
        case name
        case isLocked
    
    }
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(name, forKey: .name)
        try container.encode(isLocked, forKey: .isLocked)
      
    }
    
    public required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        isLocked = try values.decode(Bool.self, forKey: .isLocked)
        name = try values.decode(String.self, forKey: .name)
    }
    
    
}
