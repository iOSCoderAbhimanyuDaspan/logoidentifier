//
//  LogoPack.swift
//  LogoIdentifier
//
//  Created by Abhimanyu Rathore on 10/04/21.
//

import Foundation
class GamePack:Codable {
    var name: String
    var percentage:String
    var isLocked:Bool
    
    var levels:[GameLevel]
    
    init(name: String = "Pack 1",percentage:String = "0%",isLocked:Bool = true ,levels:[GameLevel] =
            [GameLevel(name: "Level 1", isLocked: false),
             GameLevel(name: "Level 2" ),
             GameLevel(name: "Level 3"),
             GameLevel(name: "Level 4"),
             GameLevel(name: "Level 5")]){
        self.levels = levels
        self.isLocked = isLocked
        self.percentage = percentage
        self.name = name
    }
    
    private enum CodingKeys: String, CodingKey{
        case name
        case percentage
        case isLocked
        case levels
    }
    
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(name, forKey: .name)
        try container.encode(percentage, forKey: .percentage)
        try container.encode(isLocked, forKey: .isLocked)
        try container.encode(levels, forKey: .levels)
    }
    
    public required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        name = try values.decode(String.self, forKey: .name)
        percentage = try values.decode(String.self, forKey: .percentage)
        isLocked = try values.decode(Bool.self, forKey: .isLocked)
        levels = try values.decode([GameLevel].self, forKey: .levels)
    }
}





