//
//  Logo.swift
//  LogoIdentifier
//
//  Created by Abhimanyu Rathore on 10/04/21.
//

import Foundation

struct Logo: Codable {
    var name: String
    var imgUrl:String
}

