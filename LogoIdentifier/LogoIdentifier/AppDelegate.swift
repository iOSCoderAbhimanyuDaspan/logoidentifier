//
//  AppDelegate.swift
//  LogoIdentifier
//
//  Created by Abhimanyu Rathore on 10/04/21.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window:UIWindow?
    


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        self.window = UIWindow.init(frame: CGRect.init(x: 0, y: 0, width: screenWidth, height: screenhright))
        
        //Initiate the game
        setRoot()
        
        return true
    }
    
    //MARK:- Game State Setting
    func setRoot(){
        if let existingGameConfig =  DBManager.getSavedGame() {
            
            //update configuration for existing things
            LogoGameConfiguration.setSavedGame(savedGame: existingGameConfig)
            
            //StoryBoard
            let gameScreen = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "GameScreenVc") as! GameScreenVc
          
            self.window = UIWindow.init(frame: CGRect.init(x: 0, y: 0, width: screenWidth, height: screenhright))
            let nav = UINavigationController.init(rootViewController: gameScreen)
            self.window?.rootViewController = nav
            
        }else{
            
            //Programatically
            let packScreen = GamePackVc()
            let nav = UINavigationController.init(rootViewController: packScreen)
            self.window?.rootViewController = nav
            
        }
        
    
        self.window?.makeKeyAndVisible()
        
        
    }

  

}

