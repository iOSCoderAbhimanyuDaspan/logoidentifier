//
//  LogoGameConfiguration.swift
//  LogoIdentifier
//
//  Created by Abhimanyu Rathore on 10/04/21.
//

import Foundation

public class LogoGameConfiguration:Codable{
 
    
    //Single tone instance
    private static var instance:LogoGameConfiguration?

    class func shared() -> LogoGameConfiguration {
        
        if self.instance == nil {
            self.instance = LogoGameConfiguration()
        }
        return self.instance!
    }
    
    
    static func removeInstance(){
        if instance != nil {
            self.instance = nil
        }
    }
    
    static func setSavedGame(savedGame:LogoGameConfiguration){
        //load on Single Tone 
        self.instance = savedGame
    }
    
    
    var gamePackes:[GamePack]
    
    //MARK:- Initializer
    private init(gamePackes:[GamePack] = [GamePack(name: "Logo Pack 1", isLocked: false),
                                  GamePack(name: "Logo Pack 2"),
                                  GamePack(name: "Logo Pack 3"),
                                  GamePack(name: "Logo Pack 4"),
                                  GamePack(name: "Logo Pack 5")]) {
        
        self.gamePackes = gamePackes
    }
    
    
    //MARK:- Helping function for encode / decode data
    private enum CodingKeys: String, CodingKey{
        case gamePackes
    
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(gamePackes, forKey: .gamePackes)
    
    }
    
    public required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        gamePackes = try values.decode([GamePack].self, forKey: .gamePackes)

    }
    
    
}
