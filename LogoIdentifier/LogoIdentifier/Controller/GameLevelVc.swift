//
//  GameLevelVc.swift
//  LogoIdentifier
//
//  Created by Abhimanyu Rathore on 10/04/21.
//

import UIKit

class GameLevelVc: UIViewController {
    
    
    lazy var tableView:UITableView = {
        let tableView = UITableView.init()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.backgroundColor = .red
        return tableView
    }()


    lazy var levels:[GameLevel] =  [GameLevel]()
    var selectedPack:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = .white
        if let _selectedPack = selectedPack {
            self.title = "Welcome in \(_selectedPack) Levels"
        }
        //set up list ui
        setTableView()
    }
}


extension GameLevelVc : UITableViewDelegate,UITableViewDataSource {
    
    func setTableView(){
        self.view.addSubview(tableView)
        
        tableView.setConstraints(item: tableView, attribute: .leading, relatedBy: .equal, toItem: self.view, attributeSec: .leading, multiplier: 1.0, constant: 0, paretnView: self.view)
        tableView.setConstraints(item: tableView, attribute: .trailing, relatedBy: .equal, toItem: self.view, attributeSec: .trailing, multiplier: 1.0, constant: 0, paretnView: self.view)
        
        tableView.setConstraints(item: tableView, attribute: .top, relatedBy: .equal, toItem: self.view, attributeSec: .top, multiplier: 1.0, constant: 0, paretnView: self.view)
        
        tableView.setConstraints(item: tableView, attribute: .bottom, relatedBy: .equal, toItem: self.view, attributeSec: .bottom, multiplier: 1.0, constant: 0, paretnView: self.view)
    }
    
    
    //MARK:- UITableViewDataSource Which is required
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        print("section \(section)")
        return levels.count
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell.init(style: .default, reuseIdentifier: "cell")
        
        print("indexPath \(indexPath)")
        
       
        print("indexPath.section \(indexPath.section)")
        print("indexPath.row \(indexPath.row)")
        
           
        cell.textLabel?.text = levels[indexPath.row].name
     
        if levels[indexPath.row].isLocked {
            cell.contentView.backgroundColor = .gray
            cell.textLabel?.textColor = .yellow
        }else{
            cell.contentView.backgroundColor = .blue
            cell.textLabel?.textColor = .white

        }
        
        return cell
    }
    
    //MARK:- UITableViewDelegate Which is Optional
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return screenWidth*0.15
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if levels[indexPath.row].isLocked == false {
     
            
            //StoryBoard
            let gameScreen = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "GameScreenVc") as! GameScreenVc

            self.navigationController?.pushViewController(gameScreen, animated: true)
        }
        
    }
    
    
  
}
